package wxapp

import "encoding/json"


type MediaType int
const (
	MTText		MediaType = 1
	MTImage 	MediaType = 2
)

type SceneType int
const (
	SCENE_DOC		SceneType = 1
	SCENE_CMT		SceneType = 2
	SCENE_FOM		SceneType = 3
	SCENE_SOC		SceneType = 4
)

type CheckTextReq struct {
	//接口版本号，2.0版本为固定值2
	Version 		string 		`json:"version"`
	//用户的openid（用户需在近两小时访问过小程序）
	OpenId 			string 		`json:"openid"`
	//场景枚举值（1 资料；2 评论；3 论坛；4 社交日志）
	Scene 			SceneType 	`json:"scene"`
	//需检测的文本内容，文本字数的上限为2500字，需使用UTF-8编码
	Content			string 		`json:"content"`
	//用户昵称，需使用UTF-8编码
	NickName 		string 		`json:"nickname"`
	//文本标题，需使用UTF-8编码
	Title 			string 		`json:"title"`
	//个性签名，该参数仅在资料类场景有效(scene=1)，需使用UTF-8编码
	Signature		string 		`json:"signature"`
}

type CheckTextRet struct {
	WxApiRet
	//唯一请求标识，标记单次请求
	TraceId 		string		`json:"trace_id"`
	//综合结果
	Result 			CheckResult	`json:"result"`
	//详细检测结果
	Detail 			[]CheckDetail	`json:"detail"`
}

type CheckResult struct {
	//建议，有risky、pass、review三种值
	Suggest 		string		`json:"suggest"`
	//命中标签枚举值，100 正常；10001 广告；20001 时政；20002 色情；20003 辱骂；20006 违法犯罪；20008 欺诈；20012 低俗；20013 版权；21000 其他
	Label 			string		`json:"label"`
}

type CheckDetail struct {
	//唯一请求标识，标记单次请求
	Strategy 		string		`json:"strategy"`
	//详细检测结果
	Errcode 		string		`json:"errcode"`
	//建议，有risky、pass、review三种值
	Suggest 		string		`json:"suggest"`
	//命中标签枚举值，100 正常；10001 广告；20001 时政；20002 色情；20003 辱骂；20006 违法犯罪；20008 欺诈；20012 低俗；20013 版权；21000 其他
	Label 			string		`json:"label"`
	//唯一请求标识，标记单次请求
	Prob	 		int			`json:"prob"`
	//命中的自定义关键词
	Keyword 		string		`json:"keyword"`
}

//检查一段文本是否含有违法违规内容。
func CheckText(access_token string, param CheckTextReq) (CheckTextRet, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/msg_sec_check"
	wx_addr += "?access_token=" +access_token

	var ret CheckTextRet
	res, err := WxApiPostStruct(wx_addr, param)
	if err != nil {
		return ret, err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return ret, err
	}

	return ret, nil
}

type CheckMediaReq struct {
	//要检测的图片或音频的url
	MediaUrl		string 		`json:"media_url"`
	//1:音频;2:图片
	MediaType 		MediaType	`json:"media_type"`
	//接口版本号，2.0版本为固定值2
	Version 		string 		`json:"version"`
	//用户的openid（用户需在近两小时访问过小程序）
	OpenId 			string 		`json:"openid"`
	//场景枚举值（1 资料；2 评论；3 论坛；4 社交日志）
	Scene 			SceneType 	`json:"scene"`
}

type CheckMediaRet struct {
	WxApiRet
	//唯一请求标识，标记单次请求
	TraceId 		string		`json:"trace_id"`
}

//异步校验图片/音频是否含有违法违规内容。
//异步检测结果在 30 分钟内会推送到你的消息接收服务器
func CheckMedia(access_token string, param CheckMediaReq) (CheckMediaRet, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/media_check_async"
	wx_addr += "?access_token=" +access_token

	var ret CheckMediaRet
	res, err := WxApiPostStruct(wx_addr, param)
	if err != nil {
		return ret, err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return ret, err
	}

	return ret, nil
}
