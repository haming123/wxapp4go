package wxapp

import (
	"encoding/json"
	"fmt"
)

type WxJumpWxa  struct {
	//通过scheme码进入的小程序页面路径
	Path 			string  	`json:"path"`
	//通过scheme码进入小程序时的query
	Query 			string  	`json:"query"`
	//要打开的小程序版本。正式版为 release，体验版为 trial，开发版为 develop
	AppEnv 			string  	`json:"env_version"`
}

type WxUrlSchemeReq struct {
	//跳转到的目标小程序信息
	Jump_wxa		WxJumpWxa 	`json:"jump_wxa"`
	//到期失效：true，永久有效：false
	IsExpire		bool      	`json:"is_expire"`
	//失效时间：0，失效间隔天数：1
	ExpireType		int     	`json:"expire_type"`
	//到期失效的 scheme 码的失效时间，为 Unix 时间戳
	ExpireTime		int64     	`json:"expire_time"`
	//到期失效的 scheme 码的失效间隔天数
	ExpireInterval	int64     	`json:"expire_interval"`
}

type WxUrlSchemeRet struct {
	WxApiRet
	Openlink 		string 		`json:"openlink"`
}

//生成页面Scheme码
//适用于短信、邮件、外部网页、微信内等拉起小程序的业务场景。
//通过该接口，可以选择生成到期失效和永久有效的小程序码，有数量限制
func CreateUrlScheme(access_token string, param WxUrlSchemeReq) (string, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/generatescheme"
	wx_addr += "?access_token=" +access_token

	var ret WxUrlSchemeRet
	res, err := WxApiPostStruct(wx_addr, param)
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return "", err
	}

	return ret.Openlink, nil
}

type SchemeInfo struct {
	//小程序 appid。
	AppId 			string  	`json:"appid"`
	//小程序页面路径。
	Path 			string  	`json:"path"`
	//小程序页面query。
	Query 			string  	`json:"query"`
	//创建时间，为 Unix 时间戳。
	CreateTime     	int64     	`json:"expire_time"`
	//到期失效时间，为 Unix 时间戳，0 表示永久生效。
	ExpireTime     	int64     	`json:"expire_time"`
	//要打开的小程序版本。正式版为"release"，体验版为"trial"，开发版为"develop"。
	AppEnv 			string  	`json:"env_version"`
}

type SchemeQuot struct {
	//长期有效 url_link 已生成次数。
	LongTimeUsed	int64     	`json:"long_time_used"`
	//长期有效 url_link 生成次数上限。
	LongTimeLimit	int64     	`json:"long_time_limit"`
}

type SchemeQueryRet struct {
	WxApiRet
	SchemeInfo 		SchemeInfo	`json:"scheme_info"`
	SchemeQuot 		SchemeQuot	`json:"scheme_quota"`
}

//查询小程序 scheme 码，及长期有效 quota
func GetSchemeInfo(access_token string, scheme string) (SchemeQueryRet, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/queryscheme"
	wx_addr += "?access_token=" +access_token

	var ret SchemeQueryRet
	paramstr :=fmt.Sprintf("{\"scheme\":\"%s\"}", scheme)
	res, err := WxApiPost(wx_addr, []byte(paramstr))
	if err != nil {
		return ret, err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return ret, err
	}

	return ret, nil
}
