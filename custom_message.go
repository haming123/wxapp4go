package wxapp

type CMType string
const (
	CMText		CMType = "text"
	CMImage 	CMType = "image"
	CMLink  	CMType = "link"
	CMAppPage  	CMType = "miniprogrampage"
)

//微信用户消息
type MediaRecive struct {
	ToUserName 		string		`json:"ToUserName"`
	FromUserName 	string		`json:"FromUserName"`
	CreateTime 		int64		`json:"CreateTime"`
	MsgType 		string		`json:"MsgType"`
	MsgId 			int64		`json:"MsgId"`
	Content 		string		`json:"Content"`
	PicUrl 			string		`json:"PicUrl"`
	MediaId 		string		`json:"MediaId"`
}

//微信消息内容对应的结构:文本消息
type MediaText struct {
	//文本消息内容
	Content 		string		`json:"content"`
}

//微信消息内容对应的结构:图片消息
type MediaImage struct {
	//发送的图片的媒体ID，通过 新增素材接口 上传图片文件获得
	MediaId 		string		`json:"media_id"`
}

//微信消息内容对应的结构:图文链接
type MediaLink struct {
	//消息标题
	Title 			string		`json:"title"`
	//图文链接消息
	Description 	string		`json:"description"`
	//图文链接消息被点击后跳转的链接
	Url 			string		`json:"url"`
	//图文链接消息的图片链接，支持 JPG、PNG 格式，较好的效果为大图 640 X 320，小图 80 X 80
	ThumbUrl 		string		`json:"thumb_url"`
}

//微信消息内容对应的结构:小程序卡片
type MediaWxApp struct {
	//消息标题
	Title 			string		`json:"title"`
	//小程序的页面路径，跟app.json对齐，支持参数，比如pages/index/index?foo=bar
	PagePath 		string		`json:"pagepath"`
	//小程序消息卡片的封面， image 类型的 media_id，通过 新增素材接口 上传图片文件获得
	ThumbMediaId 	string		`json:"thumb_media_id"`
}

//发送客服消息给用户
type CustomerMessage struct {
	//用户的 OpenID
	ToUser 			string     	`json:"touser"`
	//消息类型
	MsgType 		CMType		`json:"msgtype"`
	//文本消息，msgtype="text" 时必填
	Text 			*MediaText	`json:"text,omitempty"`
	//图片消息，msgtype="image" 时必填
	Image 			*MediaImage `json:"image,omitempty"`
	//图文链接，msgtype="link" 时必填
	Link 			*MediaLink	`json:"link,omitempty"`
	//小程序卡片，msgtype="miniprogrampage" 时必填
	AppPage 		*MediaWxApp	`json:"miniprogrampage,omitempty"`
}

//发送客服消息给用户
func SendCustomMessage(access_token string, param CustomerMessage) error {
	wx_addr := "https://api.weixin.qq.com/cgi-bin/message/custom/send"
	wx_addr += "?access_token=" + access_token
	_, err := WxApiPostStruct(wx_addr, param)
	if err != nil {
		return err
	}
	return nil
}

func SendCustomText(access_token string, touser string, text string) error {
	return SendCustomMessage(access_token, CustomerMessage{
		MsgType: CMText,
		ToUser: touser,
		Image: &MediaImage{text},
	})
}

func SendCustomImage(access_token string, touser string, media_id string) error {
	return SendCustomMessage(access_token, CustomerMessage{
		MsgType: CMImage,
		ToUser: touser,
		Text: &MediaText{media_id},
	})
}

func SendCustomLink(access_token string, touser string, title,description,url,thumb_url string) error {
	return SendCustomMessage(access_token, CustomerMessage{
		MsgType: CMLink,
		ToUser: touser,
		Link: &MediaLink{title,description,url,thumb_url },
	})
}

func SendCustomAppPage(access_token string, touser string, title,pagepath,thumb_media_id string) error {
	return SendCustomMessage(access_token, CustomerMessage{
		MsgType: CMAppPage,
		ToUser: touser,
		AppPage: &MediaWxApp{title,pagepath,thumb_media_id},
	})
}

//发送客服消息给用户
type CustomTypingReq struct {
	//用户的 OpenID
	ToUser 			string     	`json:"touser"`
	//命令
	Command 		string		`json:"command"`
}

//下发客服当前输入状态给用户
func SendCustomTyping(access_token string, touser, command string) error {
	wx_addr := "https://api.weixin.qq.com/cgi-bin/message/custom/typing"
	wx_addr += "?access_token=" + access_token
	_, err := WxApiPostStruct(wx_addr, CustomTypingReq{touser, command})
	if err != nil {
		return err
	}
	return nil
}

