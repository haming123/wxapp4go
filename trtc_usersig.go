package wxapp

import (
	"bytes"
	"compress/zlib"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/base64"
	"encoding/json"
	"strconv"
	"strings"
	"time"
)

/**
 *【功能说明】用于签发 TRTC 和 IM 服务中必须要使用的 UserSig 鉴权票据
 * sdkappid - 应用id
 * key - 计算 usersig 用的加密密钥,控制台可获取
 * userid - 用户id，限制长度为32字节，只允许包含大小写英文字母（a-zA-Z）、数字（0-9）及下划线和连词符。
 * expire - UserSig 票据的过期时间，单位是秒，比如 86400 代表生成的 UserSig 票据在一天后就无法再使用了。
 */
func GenIMUserSig(sdkappid int, key string, identifier string, expire int) (string, error) {
	currTime := time.Now().Unix()
	var sigDoc map[string]interface{}
	sigDoc = make(map[string]interface{})
	sigDoc["TLS.ver"] = "2.0"
	sigDoc["TLS.identifier"] = identifier
	sigDoc["TLS.sdkappid"] = sdkappid
	sigDoc["TLS.expire"] = expire
	sigDoc["TLS.time"] = currTime
	sigDoc["TLS.sig"] = _hmacsha256(sdkappid, key, identifier, currTime, expire)
	data, _ := json.Marshal(sigDoc)

	var b bytes.Buffer
	w := zlib.NewWriter(&b)
	w.Write(data)
	w.Close()
	return base64urlEncode(b.Bytes()), nil
}

func base64urlEncode(data []byte) string {
	str := base64.StdEncoding.EncodeToString(data)
	str = strings.Replace(str, "+", "*", -1)
	str = strings.Replace(str, "/", "-", -1)
	str = strings.Replace(str, "=", "_", -1)
	return str
}

func _hmacsha256(sdkappid int, key string, userid  string, currTime int64, expire int) string {
	content  := "TLS.identifier:" + userid  + "\n"
	content  += "TLS.sdkappid:" + strconv.Itoa(sdkappid) + "\n"
	content  += "TLS.time:" + strconv.FormatInt(currTime, 10) + "\n"
	content  += "TLS.expire:" + strconv.Itoa(expire) + "\n"
	h := hmac.New(sha256.New, []byte(key))
	h.Write([]byte(content ))
	return base64.StdEncoding.EncodeToString(h.Sum(nil))
}
