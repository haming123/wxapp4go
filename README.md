### 介绍
微信小程序服务端SDK，实现了小程序的登录、数据解密、客服消息、订阅消息、生成小程序码、生成小程序shortLink、生成页面Scheme码、生成小程序URL Link等功能。

### 安装说明
go get gitee.com/haming123/wxapp4go

### 快速上手
1. 小程序对象初始化
```go
var WxApp *wxapp.WeixinApp
func main() {
	WxApp = wxapp.NewWeixinApp("appID_string", "app_Secret", wxapp.GetAccessToken)
    //其他业务逻辑
}
```

2. 登录微信后获取用户的openid
```go
func HandlerWxLogin(w http.ResponseWriter, r *http.Request) {
	code := r.FormValue("code")
	//获取openid以及Sessionkey
	wxtoken, err := WxApp.GetOpenIdByCode(code)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte(wxtoken.OpenId))
}
```

3. 解析手机号码数据
```go
func HandlerWxPhone(w http.ResponseWriter, r *http.Request) {
	data := r.FormValue("data")
	iv := r.FormValue("iv")
	ret, err := WxApp.DecodePhoneNumber(data, "填入微信登录时获取的session_key", iv)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	fmt.Fprintf(w, ret.PhoneNumber)
}
```

4. 生成小程序码
```go
func HandlerGenQrcode(w http.ResponseWriter, r *http.Request) {
	var param wxapp.ACodeParam
	page := r.FormValue("page");
	scene := r.FormValue("scene");
	param.Path = page
	param.Scene = scene
	param.Width = 430
	raw, err := WxApp.CreateACode(param)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}

	w.Header().Set("Content-Type","image/gif")
	io.WriteString(w, string(raw))
}
```

### 使用中控服务器获取access_token
建议开发者使用中控服务器统一获取和刷新access_token，服务器所使用的access_token均来自于该中控服务器，不应该各自去刷新，否则容易造成冲突，导致access_token覆盖而影响业务。
1. 实现中控服务器
```go
package main
import (
	wxapp "gitee.com/haming123/wxapp4go"
	"net/http"
)
func HandlerGetAccessToken(w http.ResponseWriter, r *http.Request)  {
	app_id := r.FormValue("app_id")
	app_secret := r.FormValue("app_secret")
	token, err := wxapp.GetAccessToken(app_id, app_secret)
	if err != nil {
		w.WriteHeader(500)
		w.Write([]byte(err.Error()))
		return
	}
	w.Write([]byte(token))
}
func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/access_token", HandlerGetAccessToken)
	http.ListenAndServe(":8091", mux)
}
```

2. 设置getAccessToken函数
```go
var WxApp *wxapp.WeixinApp
func InitWxApp()  {
	WxApp = wxapp.NewWeixinApp("appID_string", "app_Secret", getAccessToken)
}
func getAccessToken(appid string, secret string) (string, error) {
	addr := "http://127.0.0.1:8091/access_token"
	addr = fmt.Sprintf("%s?app_id=%s&app_secret=%s", addr, appid, secret)
	res, err := http.Get(addr)
	if err != nil {
		return "", err
	}

	raw, err := ioutil.ReadAll(res.Body)
	res.Body.Close()
	if err != nil {
		return "", err
	}
	return string(raw), nil
}
```

3. 初始化小程序
```go
func main() {
	//初始化微信小程序
	InitWxApp()
    //其他业务逻辑
}
```

### 功能列表
```go
//建议使用中控服务器统一获取和刷新 access_token，因此采用自定义逻辑去获取 access_token
func NewWeixinApp(appid string, secret string, fn TokenGetFunc) *WeixinApp {
	return &WeixinApp{AppID: appid, AppSecret: secret, TokenGetter:fn}
}

//小程序数据解密
func (app *WeixinApp)WxDncrypt(rawData, iv string) (string, error) {
	return WxDncrypt(rawData, app.AppSecret, iv)
}

//根据登录凭证code获取opendid以及session_key
func (app *WeixinApp)GetOpenIdByCode(code string) (WxSessionData, error) {
	return GetOpenIdByCode(app.AppID, app.AppSecret, code)
}

//检查加密信息是否由微信生成（当前只支持手机号加密数据），只能检测最近3天生成的加密数据
func (app *WeixinApp)EncryptedCheck(encrypted_msg_hash string) (EncryptedCheckRet, error) {
	access_token := app.getAccessToken()
	return EncryptedCheck(access_token, encrypted_msg_hash)
}

//用户支付完成后，获取该用户的 UnionId，无需用户授权
func  (app *WeixinApp)GetPaidInfo(param GetPaidInfoReq) (GetPaidInfoRet, error) {
	access_token := app.getAccessToken()
	return GetPaidInfo(access_token, param)
}

//code换取用户手机号
func (app *WeixinApp)GetPhoneNumber(code string) (GetPhoneNumberRet, error) {
	access_token := app.getAccessToken()
	return GetPhoneNumber(access_token, code)
}

//解密用户手机号数据
func (app *WeixinApp)DecodePhoneNumber(rawData, key, iv string) (PhoneInfo, error) {
	return DecodePhoneNumber(rawData, key, iv)
}

//检查一段文本是否含有违法违规内容。
func (app *WeixinApp)CheckText(param CheckTextReq) (CheckTextRet, error) {
	access_token := app.getAccessToken()
	return CheckText(access_token, param)
}

//异步校验图片/音频是否含有违法违规内容。
func (app *WeixinApp)CheckMedia(param CheckMediaReq) (CheckMediaRet, error) {
	access_token := app.getAccessToken()
	return CheckMedia(access_token, param)
}

//发送客服消息给用户
func (app *WeixinApp)SendCustomMessage(param CustomerMessage) error {
	access_token := app.getAccessToken()
	return SendCustomMessage(access_token, param)
}

//发送客服消息给用户：文本消息
func (app *WeixinApp)SendCustomText(touser string, text string) error {
	access_token := app.getAccessToken()
	return SendCustomText(access_token, touser, text)
}

//发送客服消息给用户：图片消息
func (app *WeixinApp)SendCustomImage(touser string, media_id string) error {
	access_token := app.getAccessToken()
	return SendCustomImage(access_token, touser, media_id)
}

//发送客服消息给用户：图文链接
func (app *WeixinApp)SendCustomLink(touser string, title,description,url,thumb_url string) error {
	access_token := app.getAccessToken()
	return SendCustomLink(access_token, touser, title,description,url,thumb_url)
}

//发送客服消息给用户：小程序卡片
func (app *WeixinApp)SendCustomAppPage(touser string, title,pagepath,thumb_media_id string) error {
	access_token := app.getAccessToken()
	return SendCustomAppPage(access_token, touser, title,pagepath,thumb_media_id)
}

//下发客服当前输入状态给用户
func (app *WeixinApp)SendCustomTyping(touser, command string) error {
	access_token := app.getAccessToken()
	return SendCustomTyping(access_token, touser, command)
}

//把媒体文件上传到微信服务器,目前仅支持图片
func (app *WeixinApp)UploadTempMedia(file_path string) (UploadTempMediaRet, error) {
	access_token := app.getAccessToken()
	return UploadTempMedia(access_token, file_path)
}

//获取客服消息内的临时素材。即下载临时的多媒体文件。目前小程序仅支持下载图片文件。
func (app *WeixinApp)GetTempMedia(media_id string) ([]byte, error) {
	access_token := app.getAccessToken()
	return GetTempMedia(access_token, media_id)
}

//发送微信订阅消息
func (app *WeixinApp)SendSubscribeMsg(param SubscribeMsgReq) error {
	access_token := app.getAccessToken()
	return SendSubscribeMsg(access_token, param)
}

//创建被分享动态消息或私密消息的 activity_id，
func (app *WeixinApp)CreateActivity(userid string, is_unionid bool) (CreateActivityRet, error) {
	access_token := app.getAccessToken()
	return CreateActivity(access_token, userid, is_unionid)
}

//修改被分享的动态消息
func (app *WeixinApp)UpdateActivity(param UpdateActivityReq) error {
	access_token := app.getAccessToken()
	return UpdateActivity(access_token, param)
}

//通过该接口生成的小程序码，永久有效，有数量限制
func (app *WeixinApp)CreateACode(param ACodeParam) ([]byte, error) {
	access_token := app.getAccessToken()
	return CreateACode(access_token, param)
}

//通过该接口生成的小程序码，永久有效，数量暂无限制
func (app *WeixinApp)CreateACodeUnlimited(param ACodeParam) ([]byte, error) {
	access_token := app.getAccessToken()
	return CreateACodeUnlimited(access_token, param)
}

//生成页面二维码
func (app *WeixinApp)CreateQCode(page string, width int) ([]byte, error) {
	access_token := app.getAccessToken()
	return CreateQCode(access_token, page, width)
}

//生成小程序shortLink
func (app *WeixinApp)CreateShortLink(param ShortLinkReq) (string, error) {
	access_token := app.getAccessToken()
	return CreateShortLink(access_token, param)
}

//生成临时shortLink
func (app *WeixinApp)CreateShortLinkTemp(page_url string, page_title string) (string, error) {
	access_token := app.getAccessToken()
	return CreateShortLinkTemp(access_token, page_url, page_title)
}

//生成永久shortLink
func (app *WeixinApp)CreateShortLinkPermanent(page_url string, page_title string) (string, error) {
	access_token := app.getAccessToken()
	return CreateShortLinkPermanent(access_token, page_url, page_title)
}

//生成页面Scheme码
func (app *WeixinApp)CreateUrlScheme(param WxUrlSchemeReq) (string, error) {
	access_token := app.getAccessToken()
	return CreateUrlScheme(access_token, param)
}

//查询小程序 scheme 码，及长期有效 quota
func (app *WeixinApp)GetSchemeInfo(scheme string) (SchemeQueryRet, error) {
	access_token := app.getAccessToken()
	return GetSchemeInfo(access_token, scheme)
}

//生成小程序URL Link，
func (app *WeixinApp)CreateUrlLink(param UrlLinkReq) (string, error) {
	access_token := app.getAccessToken()
	return CreateUrlLink(access_token, param)
}

//查询小程序 url_link 配置，及长期有效 quota
func (app *WeixinApp)GetUrlLinkInfo(url_link string) (UrlLinkQueryRet, error) {
	access_token := app.getAccessToken()
	return GetUrlLinkInfo(access_token, url_link)
}

//用于签发 TRTC 和 IM 服务中必须要使用的 UserSig 鉴权票据
func (app *WeixinApp)GenIMUserSig(sdkappid int, key string, identifier string, expire int) (string, error) {
	return GenIMUserSig(sdkappid, key, identifier, expire)
}
```