package wxapp

import (
	"fmt"
)

const ENV_R = "release"
const ENV_T = "trial"
const ENV_D = "develop"

type RGBColor struct {
	R int `json:"r"`
	G int `json:"g"`
	B int `json:"b"`
}

type ACodeParam struct {
	//扫码进入的小程序页面路径，最大长度 128 字节
	Path string `json:"path"`
	//最大32个可见字符，只支持数字，大小写英文以及部分特殊字符
	Scene string `json:"scene,omitempty"`
	//检查 page 是否存在，为 true 时 page 必须是已经发布的小程序存在的页面（否则报错）；
	//为 false 时允许小程序未发布或者 page 不存在， 但 page 有数量上限
	CheckPath string `json:"check_path,omitempty"`
	//要打开的小程序版本。正式版为 release，体验版为 trial，开发版为 develop
	AppEnv string `json:"env_version,omitempty"`
	//二维码的宽度，单位 px。最小 280px，最大 1280px
	Width int `json:"width,omitempty"`
	//自动配置线条颜色，如果颜色依然是黑色，则说明不建议配置主色调
	AutoColor bool `json:"auto_color,omitempty"`
	//auto_color 为 false 时生效，使用 rgb 设置颜色
	LineColor RGBColor `json:"line_color,omitempty"`
	//是否需要透明底色，为 true 时，生成透明底色的小程序码
	IdHyaline bool `json:"is_hyaline,omitempty"`
}

//获取小程序码
//适用于需要的码数量较少的业务场景。
//通过该接口生成的小程序码，永久有效，有数量限制
func CreateACode(access_token string, param ACodeParam) ([]byte, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/getwxacode"
	wx_addr += "?access_token=" + access_token
	res, err := WxApiPostStruct(wx_addr, param)
	if err != nil {
		return nil, err
	}
	return res, nil
}

//获取小程序码，
//适用于需要的码数量极多的业务场景。
//通过该接口生成的小程序码，永久有效，数量暂无限制
func CreateACodeUnlimited(access_token string, param ACodeParam) ([]byte, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/getwxacodeunlimit"
	wx_addr += "?access_token=" + access_token
	res, err := WxApiPostStruct(wx_addr, param)
	if err != nil {
		return nil, err
	}
	return res, nil
}

type CreateQCodeReq struct {
	//扫码进入的小程序页面路径，最大长度 128 字节
	Path string `json:"path"`
	//二维码的宽度，单位 px。最小 280px，最大 1280px
	Width int `json:"width"`
}

//生成页面二维码
//page：扫码进入的小程序页面路径，最大长度 128 字节
//width：二维码的宽度，单位 px。最小 280px，最大 1280px
func CreateQCode(access_token string, page string, width int) ([]byte, error) {
	wx_addr := "https://api.weixin.qq.com/cgi-bin/wxaapp/createwxaqrcode"
	wx_addr += "?access_token=" + access_token
	paramstr := fmt.Sprintf("{\"path\":\"%s\", \"width\":%d}", page, width)
	res, err := WxApiPost(wx_addr, []byte(paramstr))
	if err != nil {
		return nil, err
	}
	return res, nil
}
