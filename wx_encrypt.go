package wxapp

import (
	"crypto/aes"
	"crypto/cipher"
	"encoding/base64"
)

//解除KCS#7填充
func PKCS7UnPadding(origData []byte) []byte {
	length := len(origData)
	unpadding := int(origData[length-1])
	return origData[:(length - unpadding)]
}

//AES-128-CBC解密
func AesCBCDncrypt(encryptData, key, iv []byte) ([]byte, error) {
	block, err := aes.NewCipher(key)
	if err != nil {
		panic(err)
	}
	blockSize := block.BlockSize()
	if len(encryptData) < blockSize {
		panic("ciphertext too short")
	}
	if len(encryptData)%blockSize != 0 {
		panic("ciphertext is not a multiple of the block size")
	}
	mode := cipher.NewCBCDecrypter(block, iv)
	mode.CryptBlocks(encryptData, encryptData)

	//解填充
	encryptData = PKCS7UnPadding(encryptData)
	return encryptData, nil
}

//解密算法如下：
//1)对称解密使用的算法为 AES-128-CBC，数据采用PKCS#7填充。
//2)对称解密的目标密文为 Base64_Decode(encryptedData)。
//3)对称解密秘钥 aeskey = Base64_Decode(session_key)。
//4)对称解密算法初始向量 为Base64_Decode(iv)，其中iv由数据接口返回。
func WxDncrypt(rawData, key, iv string) (string, error) {
	data, err := base64.StdEncoding.DecodeString(rawData)
	if err != nil {
		return "", err
	}
	key_b, err := base64.StdEncoding.DecodeString(key)
	if err != nil {
		return "", err
	}
	iv_b, err := base64.StdEncoding.DecodeString(iv)
	if err != nil {
		return "", err
	}
	dnData, err := AesCBCDncrypt(data, key_b, iv_b)
	if err != nil {
		return "", err
	}
	return string(dnData), nil
}
