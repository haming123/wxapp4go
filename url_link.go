package wxapp

import (
	"encoding/json"
	"fmt"
)

type CloudParam  struct {
	//云开发环境
	Env 			string  	`json:"env"`
	//静态网站自定义域名，不填则使用默认域名
	Domain 			string  	`json:"domain"`
	//云开发静态网站 H5 页面路径，不可携带 query
	Path 			string  	`json:"path"`
	//云开发静态网站 H5 页面 query 参数，最大 1024 个字符，只支持数字，大小写英文以及部分特殊字符
	Query 			string  	`json:"query"`
	//第三方批量代云开发时必填，表示创建该 env 的 appid
	ResourceAppId	string  	`json:"resource_appid"`
}

type UrlLinkReq struct {
	//通过 URL Link 进入的小程序页面路径，必须是已经发布的小程序存在的页面，不可携带 query 。path 为空时会跳转小程序主页
	Path 			string  	`json:"path"`
	//通过 URL Link 进入小程序时的query，最大1024个字符，只支持数字，大小写英文以及部分特殊字符
	Query 			string  	`json:"query"`
	//要打开的小程序版本。正式版为 release，体验版为 trial，开发版为 develop
	AppEnv 			string  	`json:"env_version"`
	//生成的 URL Link 类型，到期失效：true，永久有效：false。
	IsExpire       	bool      	`json:"is_expire"`
	//小程序 URL Link 失效类型，失效时间：0，失效间隔天数：1
	ExpireType     	int64     	`json:"expire_type"`
	//到期失效的 scheme 码的失效时间，为 Unix 时间戳
	ExpireTime     	int64     	`json:"expire_time"`
	//到期失效的 scheme 码的失效间隔天数
	ExpireInterval 	int64     	`json:"expire_interval"`
	//云开发静态网站自定义 H5 配置参数，可配置中转的云开发 H5 页面。不填默认用官方 H5 页面
	CloudBase       CloudParam 	`json:"cloud_base"`
}

type UrlLinkRet struct {
	WxApiRet
	UrlLink 		string 		`json:"url_link"`
}

//获取小程序 URL Link，
//适用于短信、邮件、网页、微信内等拉起小程序的业务场景
//通过该接口，可以选择生成到期失效和永久有效的小程序链接，有数量限制
func CreateUrlLink(access_token string, param UrlLinkReq) (string, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/generate_urllink"
	wx_addr += "?access_token=" +access_token

	var ret UrlLinkRet
	res, err := WxApiPostStruct(wx_addr, param)
	if err != nil {
		return "", err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return "", err
	}

	return ret.UrlLink, nil
}

type UrlLinkInfo struct {
	//小程序 appid。
	AppId 			string  	`json:"appid"`
	//小程序页面路径。
	Path 			string  	`json:"path"`
	//小程序页面query。
	Query 			string  	`json:"query"`
	//创建时间，为 Unix 时间戳。
	CreateTime     int64     	`json:"expire_time"`
	//到期失效时间，为 Unix 时间戳，0 表示永久生效。
	ExpireTime     int64     	`json:"expire_time"`
	//要打开的小程序版本。正式版为"release"，体验版为"trial"，开发版为"develop"。
	AppEnv 			string  	`json:"env_version"`
	//云开发静态网站自定义 H5 配置参数，可配置中转的云开发 H5 页面。不填默认用官方 H5 页面
	CloudBase       CloudParam 	`json:"cloud_base"`
}

type UrlLinkQuot struct {
	//长期有效 url_link 已生成次数。
	LongTimeUsed	int64     	`json:"long_time_used"`
	//长期有效 url_link 生成次数上限。
	LongTimeLimit	int64     	`json:"long_time_limit"`
}

type UrlLinkQueryRet struct {
	ErrCode 		int64  		`json:"errcode"`
	ErrMsg  		string 		`json:"errmsg"`
	UrlLinkInfo 	UrlLinkInfo	`json:"url_link_info"`
	UrlLinkQuot 	UrlLinkQuot	`json:"url_link_quota"`
}

//查询小程序 url_link 配置，及长期有效 quota
func GetUrlLinkInfo(access_token string, url_link string) (UrlLinkQueryRet, error) {
	wx_addr := "https://api.weixin.qq.com/wxa/query_urllink"
	wx_addr += "?access_token=" +access_token

	var ret UrlLinkQueryRet
	paramstr := fmt.Sprintf("{\"url_link\":\"%s\"}", url_link)
	res, err := WxApiPost(wx_addr, []byte(paramstr))
	if err != nil {
		return ret, err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return ret, err
	}

	return ret, nil
}
