package wxapp

import (
	"encoding/json"
	"errors"
)

type CreateActivityRet struct {
	WxApiRet
	//动态消息的 ID
	ActivityId 		string 		`json:"activity_id"`
	//activity_id 的过期时间戳。默认24小时后过期。
	ExpirationTime  int64     	`json:"expiration_time"`
}

//创建被分享动态消息或私密消息的 activity_id，
func CreateActivity(access_token string, userid string, is_unionid bool) (CreateActivityRet, error) {
	wx_addr := "https://api.weixin.qq.com/cgi-bin/message/wxopen/activityid/create"
	wx_addr += "?access_token=" +access_token
	if is_unionid {
		wx_addr += "&unionid=" + userid
	} else {
		wx_addr += "&openid=" + userid
	}

	var ret CreateActivityRet
	res, err := WxApiGet(wx_addr)
	if err != nil {
		return ret, err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return ret, err
	}

	return ret, nil
}

/*
member_count	target_state = 0 时必填，文字内容模板中 member_count 的值
room_limit	target_state = 0 时必填，文字内容模板中 room_limit 的值
path	target_state = 1 时必填，点击「进入」启动小程序时使用的路径。
version_type	target_state = 1 时必填，有效参数值为：develop（开发版），trial（体验版），release（正式版）
*/
type TemplValue struct {
	Name 			string 		`json:"name"`
	Value 			string 		`json:"value"`
}

type TemplInfo struct {
	//模板中需要修改的参数
	Items		[]TemplValue	`json:"parameter_list"`
}

type UpdateActivityReq struct {
	//动态消息的 ID
	ActivityId 		string 		`json:"activity_id"`
	//动态消息修改后的状态： 0 未开始 1已开始
	TargetState 	int  		`json:"target_state"`
	//动态消息对应的模板信息
	TemplInfo   	TemplInfo	`json:"template_info"`
}

//修改被分享的动态消息
//消息有两个状态（target_state），分别有其对应的文字内容和颜色。文字内容模板和颜色不支持变更。
//0	"成员正在加入，当前 {member_count}/{room_limit} 人"
//1	"已开始"
func UpdateActivity(access_token string, param UpdateActivityReq) error {
	wx_addr := "https://api.weixin.qq.com/cgi-bin/message/wxopen/updatablemsg/send"
	wx_addr += "?access_token=" +access_token

	var ret WxApiRet
	res, err := WxApiPostStruct(wx_addr, param)
	if err != nil {
		return err
	}

	err = json.Unmarshal(res, &ret)
	if err != nil {
		return err
	}

	if ret.ErrCode == 0 {
		return nil
	} else {
		return errors.New(ret.ErrMsg)
	}
}